<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package gulp-wordpress
 */
get_header(); ?>
<div class="move-content"></div>
<div id="primary" class="content-area">
   <main id="main" class="site-main" role="main">
      <div class="container section">


         <div class="post-inner-content ">

            
            <h1 class='post-title uppercase text-center color-red'>
					Seite nicht gefunden - Fehler 404 
            </h1>
            <br><br><br><br><br><br>
         </div>
      </div>

      <div class="news-page section">
         <div class="row">
				<div class="container">

				<div class="col-12">
					<h1 class="text-center section-title uppercase">ähnlich NEWS</h1>
				</div>

               <?php
				$the_query = new WP_Query( array(
					'posts_per_page' =>3,
				)); 
			if ( $the_query->have_posts() ) : ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

						<?php get_template_part( 'template-parts/content', 'page' ); ?>

						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>

						<?php else : ?>
						<p><?php __('No News'); ?></p>
						<?php endif; ?>
					</div>åå


         </div>

   </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();