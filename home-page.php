<?php /* Template Name: Home-page */ get_header(); ?>

<?php the_field( 'uber_uns' ); ?>

<?php the_field( 'ein_kurzer_uberblick_des_jahres' ); ?>

<?php the_field( 'reding_kapell-stiftung' ); ?>

<?php if ( have_rows( 'board_members', 'option' ) ) : ?>

<section class="container board-members section">
   <div class="row ">
      <div class="col-12">
         <h3 class="text-center uppercase color-light-red section-title">Vorstandsmitgliede</h3>
      </div>
      <?php while ( have_rows( 'board_members', 'option' ) ) : the_row(); ?>
      <div class="col-12 col-sm-6 col-md-4 member">
         <div>
            <img class="label" src="<?php echo get_template_directory_uri() ?>/img/fill.png" alt="">
            <div class="img">
               <?php $image = get_sub_field( 'image' ); ?>
               <?php if ( $image ) { ?>
               <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
               <?php } ?>
               <h4><?php the_sub_field( 'name' ); ?></h4>
               <span><?php the_sub_field( 'title' ); ?></span>
            </div>
         </div>
      </div>
      <?php endwhile; ?>
      <?php endif; ?>
   </div>
</section>

<?php the_field( 'veranstaltungen' ); ?>

<?php the_field( 'newsletter_und_unterstutzen' ); ?>

<!-- latest news -->
<section class="container latest-news section">
   <h2 class="text-center section-title">NEWS</h2>
   <?php
      $the_query = new WP_Query( array(
         'posts_per_page' => 2,
      )); 
      if ( $the_query->have_posts() ) : ?>
   <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

   <?php get_template_part( 'template-parts/content', 'page' ); ?>

   <?php endwhile; ?>
   <?php wp_reset_postdata(); ?>

   <?php else : ?>
   <p><?php __('No News'); ?></p>
   <?php endif; ?>

   <a href="/news" class="btn btn-dark btn-center">
      <span>Weiterlesen</span>
   </a>
</section>
<!-- end latest news -->

<!-- location map -->
<section class="container section relative line">
   <div class="row">
      <div class="col-12">
         <h2 class="text-center section-title">LOKALISIERUNG</h2>
      </div>
      <div class="col-12 map-wrap">
         <div id="map"></div>
      </div>
   </div>
   <!-- end row -->
</section>
<!-- end container -->

<?php get_footer(); ?>