<?php
/**
 * gulp-wordpress functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gulp-wordpress
 */

if ( ! function_exists( 'gulp_wordpress_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function gulp_wordpress_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on gulp-wordpress, use a find and replace
	 * to change 'gulp-wordpress' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'gulp-wordpress', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'gulp-wordpress' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'gulp_wordpress_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'gulp_wordpress_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gulp_wordpress_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'gulp_wordpress_content_width', 640 );
}
add_action( 'after_setup_theme', 'gulp_wordpress_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gulp_wordpress_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gulp-wordpress' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gulp-wordpress' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gulp_wordpress_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gulp_wordpress_scripts() {

	// Bootstrap css
	wp_enqueue_style( 'bootstrap-css',  get_template_directory_uri() . '/css/bootstrap.min.css' );

	// style.css
	wp_enqueue_style( 'gulp-wordpress-style', get_stylesheet_uri() );

	// Flexslider
	wp_enqueue_style( 'flexslider-css',  get_template_directory_uri() . '/css/flexslider.css' );

	// Mapbox js
	wp_enqueue_script( 'map_box_js', 'https://api.mapbox.com/mapbox-gl-js/v0.54.0/mapbox-gl.js', array(), '2', true );
	
	// Mapbox js
	wp_enqueue_script( 'map_box_js_js', get_template_directory_uri() . '/js/map_box.js', array(), '2', true );

	// Poper js
	wp_enqueue_script( 'poper-js','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array(), '2', true );

	// Bootstrap js
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '2', true );

	// Navigation js
	wp_enqueue_script( 'navigation', get_template_directory_uri() . '/js/navigation.js', array(), '2', true );

	// Flexslider js
	wp_enqueue_script( 'flexslider-js', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array(), '2', true );

	// waypoint.js
	wp_enqueue_script( 'waypoint', get_template_directory_uri() . '/js/waypoints.min.js', array(), '3', true );

	// placeholder animation
	wp_enqueue_script( 'form-placeholder', get_template_directory_uri() . '/js/form-placeholder.js', array(), '2', true );
	// timeline
	wp_enqueue_script( 'timeline-js', get_template_directory_uri() . '/js/timeline.js', array(), '2', true );

	// main.js
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array(), '2', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gulp_wordpress_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


// Register Custom Post Type
function cpt_CustomPost() {

	$labels = array(
		'name'                  => _x( 'Sermons', 'Job General Name', 'gulp_wordpress' ),
		'singular_name'         => _x( 'Sermons', 'Job Singular Name', 'gulp_wordpress' ),
		'menu_name'             => __( 'Sermons', 'gulp_wordpress' ),
		'name_admin_bar'        => __( 'Sermons', 'gulp_wordpress' ),
		'archives'              => __( 'Item Archives', 'gulp_wordpress' ),
		'attributes'            => __( 'Item Attributes', 'gulp_wordpress' ),
		'parent_item_colon'     => __( 'Parent Item:', 'gulp_wordpress' ),
		'all_items'             => __( 'All Items', 'gulp_wordpress' ),
		'add_new_item'          => __( 'Add New Item', 'gulp_wordpress' ),
		'add_new'               => __( 'Add New', 'gulp_wordpress' ),
		'new_item'              => __( 'New Item', 'gulp_wordpress' ),
		'edit_item'             => __( 'Edit Item', 'gulp_wordpress' ),
		'update_item'           => __( 'Update Item', 'gulp_wordpress' ),
		'view_item'             => __( 'View Item', 'gulp_wordpress' ),
		'view_items'            => __( 'View Items', 'gulp_wordpress' ),
		'search_items'          => __( 'Search Item', 'gulp_wordpress' ),
		'not_found'             => __( 'Not found', 'gulp_wordpress' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'gulp_wordpress' ),
		'featured_image'        => __( 'Featured Image', 'gulp_wordpress' ),
		'set_featured_image'    => __( 'Set featured image', 'gulp_wordpress' ),
		'remove_featured_image' => __( 'Remove featured image', 'gulp_wordpress' ),
		'use_featured_image'    => __( 'Use as featured image', 'gulp_wordpress' ),
		'insert_into_item'      => __( 'Insert into item', 'gulp_wordpress' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'gulp_wordpress' ),
		'items_list'            => __( 'Items list', 'gulp_wordpress' ),
		'items_list_navigation' => __( 'Items list navigation', 'gulp_wordpress' ),
		'filter_items_list'     => __( 'Filter items list', 'gulp_wordpress' ),
	);
	$args = array(
		'label'                 => __( 'Sermons', 'gulp_wordpress' ),
		'description'           => __( 'Sermons Description', 'gulp_wordpress' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'main titile', 'author' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon'   => 'dashicons-megaphone',
		'show_in_rest' => true,
	);
	register_post_type( 'sermons-post', $args );

}

add_action( 'init', 'cpt_CustomPost', 0 );



//* ======== EXCERTP ======== *//
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


//* =========== ACF ============ */

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
		
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme contact Settings',
		'menu_title'	=> 'Contact',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme contact Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
}