<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		
			<!-- <div data-scroll class="page page--layout-2 container">
			
				<div class="content content--full content--alternate row">
				
					<div class="col-6">
						<span class="content__item-number">01</span>
						<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Architecto cumque voluptatibus odit ducimus corporis voluptates deserunt molestias aliquid eligendi quos nostrum maiores eum, vero commodi possimus, ut blanditiis tenetur sunt.</p>
					</div>

					<div class="content__item content__item--wide col-6">
						<span class="content__item-number">01</span>
						<div class="content__item-imgwrap">
							<div class="content__item-img" style="background-image: url(https://picsum.photos/900/700.jpg);"></div>
						</div>
						<div class="content__item-deco"></div>
					</div>

					<div class="col-6">
						<span class="content__item-number">01</span>
						<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Architecto cumque voluptatibus odit ducimus corporis voluptates deserunt molestias aliquid eligendi quos nostrum maiores eum, vero commodi possimus, ut blanditiis tenetur sunt.</p>
					</div>

					<div class="content__item content__item--wide col-6">
						<span class="content__item-number">01</span>
						<div class="content__item-imgwrap"><div class="content__item-img" style="background-image: url(https://picsum.photos/900/700.jpg);"></div></div>
						<div class="content__item-deco"></div>
					</div>

						<div class="col-6">
						<span class="content__item-number">01</span>
						<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Architecto cumque voluptatibus odit ducimus corporis voluptates deserunt molestias aliquid eligendi quos nostrum maiores eum, vero commodi possimus, ut blanditiis tenetur sunt.</p>
					</div>

					<div class="content__item content__item--wide col-6">
						<span class="content__item-number">01</span>
						<div class="content__item-imgwrap"><div class="content__item-img" style="background-image: url(https://picsum.photos/900/700.jpg);"></div></div>
						<div class="content__item-deco"></div>
					</div>

						<div class=" col-6">
						<span class="content__item-number">01</span>
						<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Architecto cumque voluptatibus odit ducimus corporis voluptates deserunt molestias aliquid eligendi quos nostrum maiores eum, vero commodi possimus, ut blanditiis tenetur sunt.</p>
					</div>

					<div class="content__item content__item--wide col-6">
						<span class="content__item-number">01</span>
						<div class="content__item-imgwrap"><div class="content__item-img" style="background-image: url(https://picsum.photos/900/700.jpg);"></div></div>
						<div class="content__item-deco"></div>
					</div>
				</div>
			</div> -->
			<?php
			while ( have_posts() ) : the_post();

				the_content();
				// get_template_part( 'template-parts/content', 'page' );
			
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
