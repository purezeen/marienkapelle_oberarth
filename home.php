<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

get_header(); ?>

<!-- hero section -->
<section class="hero cover" style="background-image: url(<?php echo get_template_directory_uri() ?>/img/hero-news.png">
</section>
<!-- end hero section -->

<div class="container news-page">
   <div class="row">
      <div class="col-12">
         <h1 class="text-center section-title">BLOG</h1>
      </div>
   </div>

   <?php
   while ( have_posts() ) : the_post();

      get_template_part( 'template-parts/content', 'page' );
   
   endwhile; // End of the loop.
   ?>
   
   <!-- end row -->
</div>
<!-- end container -->
   
<div class="row">
      <div class="col-12 pagination text-center line">
         <?php
            the_posts_pagination( array(
               'mid_size'  => 1,
               'prev_text' => __( 'bisherige', '' ),
               'next_text' => __( 'nächster', '' ),
            ) );
         ?>
      </div>
</div>

<?php
get_footer();
