(function ($) {

  /* ========= ANIMATION FORM PLACEHOLDER ========= */
  $("div.wpcf7 input").focusin(function () {
    $(this).parent().next().css('top', '2px');
    $(this).parent().next().css('font-size', '12px');
  });

  $("div.wpcf7 span input").focusout(function () {
    if ($(this).val().length === 0) {
      $(this).parent().next().css('top', '17px');
      $(this).parent().next().css('font-size', '15px');

    } else {
      $(this).parent().next().css('top', '2px');
      $(this).parent().next().css('font-size', '12px');
    }
  });

  $("div.wpcf7 textarea").focusin(function () {
    $(this).parent().next().css('top', '2px');
    $(this).parent().next().css('font-size', '12px');
  });

  $("div.wpcf7 span textarea").focusout(function () {
    if ($(this).val().length === 0) {
      $(this).parent().next().css('top', '17px');
      $(this).parent().next().css('font-size', '15px');

    } else {
      $(this).parent().next().css('top', '2px');
      $(this).parent().next().css('font-size', '12px');
    }
  });

})(jQuery);