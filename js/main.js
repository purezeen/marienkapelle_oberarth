(function ($) {

  jQuery(document).ready(function ($) {

    $(document).ready(function () {
      $('.site-header .dropdown-toggle').click(function () {
        var location = $(this).attr('href');
        window.location.href = location;
        return false;
      });
    });

    $(document).ready(function () {

      if ($(document).width() <= 990) { // If page is scrolled more than 50px
        $('.site-header .disable .dropdown-toggle').click(function () {
          $(this).parent('.dropdown').find('.dropdown-menu').slideToggle(); 
        });

        $('.site-header .hover-nav-link').append('<span class="nav-arrow"><i></i></span>');

        $('.site-header .hover-nav-link .nav-arrow').click(function () {
          $(this).parent('.dropdown').find('.dropdown-menu').slideToggle(); 
        });
      } 

    });

    setTimeout(function () {
      $('body').addClass('loaded');
      $('h1').css('color', '#222222');
    }, 2000);

    // ===== Scroll to Top ==== 
    $(window).scroll(function () {
      if ($(this).scrollTop() >= 200) { // If page is scrolled more than 50px
        $('.scroll').fadeIn(200); // Fade in the arrow
      } else {
        $('.scroll').fadeOut(200); // Else fade out the arrow
      }
    });

    $('.scroll').click(function () {
      $('html, body').animate({
        scrollTop: 0
      }, 800);
      return false;
    }); // click() scroll top EMD	


    /* ========= SMOOT SCROLL ========= */
    $('a[href*=\\#]:not([href=\\#])').click(function () {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 1500);
          return false;
        }
      }
    });
    /* ========= END SMOOT SCROLL ========= */

    /* ========= NUMBER COUNTER ========= */

    if ($("body").hasClass("home")) {
      var waypoint = new Waypoint({
        element: document.getElementById('number-counter'),
        handler: function () {

          $('.count').each(function () {
            var size = $(this).text().split(".")[1] ? $(this).text().split(".")[1].length : 0;
            $(this).prop('Counter', 0).animate({
              Counter: $(this).text()
            }, {
              duration: 2000,
              step: function (func) {
                $(this).text(parseFloat(func).toFixed(size));
              }
            });
          });

          $('.border-box').each(function () {

          });

          this.destroy()
        },
        offset: '70% '
      });


      $(document).ready(function () {
        var waypoint = new Waypoint({
          element: document.getElementById('number-counter'),
          handler: function () {
            window.addEventListener('scroll', function () {
              var scrollPosition = window.pageYOffset;
              var paralax_container = document.getElementById('number-counter');
              var limit = paralax_container.offsetTop + paralax_container.offsetHeight;
            });
          },
          offset: '70% '
        });

        $('.modal-content').click(function (event) {
          event.stopPropagation();
      });

      });
    }
    /* ========= END NUMBER COUNTER ========= */

  });

})(jQuery);