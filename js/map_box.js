(function ($) {

  $(document).ready(function () {

    if($('body').is('.home, .page-template-contact')) {

    // This will let you use the .remove() function later on
    if (!('remove' in Element.prototype)) {
      Element.prototype.remove = function () {
        if (this.parentNode) {
          this.parentNode.removeChild(this);
        }
      };
    }

    mapboxgl.accessToken = 'pk.eyJ1Ijoic291bGxpZ2h0ZXIiLCJhIjoiY2o2NTJiZTgxMXptcDMycTd6aThseGZxeSJ9.QLx69TXSXduB5oFoICZpxQ';

    // This adds the map
    var map = new mapboxgl.Map({
      // container id specified in the HTML
      container: 'map',
      // style URL
      style: 'mapbox://styles/soullighter/cjzjr0p04030z1cpblo75cahl',
      // initial position in [long, lat] format
      center: [8.5342185, 47.056776],
      // initial zoom
      zoom: 15,
    });

    // disable map zoom when using scroll
    // map.scrollZoom.disable();

    map.on("load", function () {
         // var website_url = document.location.host + '/wp-content/themes/church-theme/img/map-marker.png';
      /* Image: An image is loaded and added to the map. */
      map.loadImage("http://marienkirche-oberarth.flywheelsites.com/wp-content/themes/church-theme/img/map-marker.png", function(error, image) {
          if (error) throw error;
          map.addImage("custom-marker", image);
          /* Style layer: A style layer ties together the source and image and specifies how they are displayed on the map. */
          map.addLayer({
            id: "markers",
            type: "symbol",
            /* Source: A data source specifies the geographic coordinate where the image marker gets placed. */
            source: {
              type: "geojson",
              data: {
                type: 'FeatureCollection',
                features: [
                  {
                    type: 'Feature',
                    properties: {},
                    geometry: {
                      type: "Point",
                      coordinates: [8.5342185, 47.056776]
                    }
                  }
                ]
              }
            },
            layout: {
              "icon-image": "custom-marker",
            }
          });
        });
    });
  }
  });
})(jQuery);