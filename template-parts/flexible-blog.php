<?php if ( have_rows( 'two_columns' ) ): ?>
	<?php while ( have_rows( 'two_columns' ) ) : the_row(); ?>
      <?php if ( get_row_layout() == 'content_with_image' ) : ?>
      
			<?php if ( get_sub_field( 'set_image_to_right_column' ) == 1 ) { 
         ?>
         <div class="row two-columns-content">
            <div class="col-12 col-lg-6 content-left">
               <?php the_sub_field( 'text' ); ?>
            </div>
            <div class="col-12 col-lg-6 img-wrap-16-9 cover">
               <?php $image = get_sub_field( 'image' ); ?>
               <?php if ( $image ) { ?>
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
               <?php } ?>
            </div>
         </div>
         <?php
			} else { 
            ?>
            <div class="row two-columns-content">

            <div class="col-12 col-lg-6 img-wrap-16-9 cover">
               <?php $image = get_sub_field( 'image' ); ?>
               <?php if ( $image ) { ?>
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
               <?php } ?>
            </div>
            <div class="col-12 col-lg-6 content-right">
               <?php the_sub_field( 'text' ); ?>
            </div>
            
         </div>
         <?php
         } ?>
      
      <?php elseif ( get_row_layout() == 'one_column' ) : ?>

         <div class="row">
            <div class="col-12 one-column">
               <?php the_sub_field( 'text' ); ?>
            </div>
         </div>
			
      <?php endif; ?>
      

	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>
