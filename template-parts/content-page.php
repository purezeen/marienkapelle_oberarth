<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

?>

	<a href="<?php echo get_permalink();?>" id="post-<?php the_ID(); ?>" <?php post_class('row horizontal-cart margin-b'); ?> href="<?php echo get_permalink();?>">

		<?php
			if (has_post_thumbnail()) {
			$backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); 
			}
		?>
		<div class="red-frame col-12 col-sm-3 col-md-6 p-0 image">
			<div class="cover horizontal-shape" style="background-image: url('<?php echo $backgroundImg[0]?>')">
			</div>
		</div>
		
		<div class="col-12 col-sm-9 col-md-6 inner-content">
			<span class="color-light-red date"><?php echo get_the_date(); ?></span>
			<h3 class="uppercase color-red"><?php the_title(); ?> </h3>
			<p><?php the_excerpt(); ?> </p>
			<div class="btn-link btn-right mt-3">Geschichte</div>
		</div>
		
	</a><!-- #post-## -->


