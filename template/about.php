<?php /* Template Name: About */ get_header(); ?>


<?php the_field( 'marienkirche_oberarth' ); ?>


<?php the_field( 'seelsorger' ); ?>

<?php the_field( 'reding_kapell_stiftung' ); ?>

<?php if ( have_rows( 'board_members', 'option' ) ) : ?>
<section class="container board-members section">
   <div class="row ">
      <div class="col-12">
         <h3 class="text-center uppercase color-light-red section-title">Vorstandsmitgliede</h3>
      </div>
      <?php while ( have_rows( 'board_members', 'option' ) ) : the_row(); ?>
      <div class="col-12 col-sm-6 col-md-4 member">
         <div>
            <img class="label" src="<?php echo get_template_directory_uri() ?>/img/fill.png" alt="">
            <div class="img">
               <?php $image = get_sub_field( 'image' ); ?>
               <?php if ( $image ) { ?>
               <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
               <?php } ?>
               <h4><?php the_sub_field( 'name' ); ?></h4>
               <span><?php the_sub_field( 'title' ); ?></span>
            </div>
         </div>
      </div>
      <?php endwhile; ?>
      <?php endif; ?>
   </div>
</section>

<?php the_field( 'hast_du_fragen' ); ?>



<div class="main-content-inner history-slider-wrap container section" id="history">

   <h2 class="text-center">GESCHICHTE</h2>

   <section class="cd-horizontal-timeline">
      <div class="timeline">
         <div class="events-wrapper">
            <div class="events">
               <?php if ( have_rows( 'geschichte' ) ) : 
            $i=0;
          ?>
               <ol>
                  <?php while ( have_rows( 'geschichte' ) ) : the_row(); ?>
                  <li><a href="#0" data-date="01/01/<?php the_sub_field( 'year' ); ?>"
                        <?php echo 'class="' . ($i==0 ? 'selected' : '') . '"'; ?>><?php the_sub_field( 'year' ); ?></a>
                  </li>
                  <?php $i++;  ?>
                  <?php endwhile; 
            ?>
               </ol>
               <?php endif; ?>
               <span class="filling-line" aria-hidden="true"></span>
            </div> <!-- .events -->
         </div> <!-- .events-wrapper -->
      </div> <!-- .timeline -->

      <ul class="cd-timeline-navigation">
         <li><a href="#0" class="slide-btn prev inactive"><img
                  src="<?php echo get_template_directory_uri() ?>/img/slider-arrow-left.svg" alt=""></a></li>
         <li><a href="#0" class="slide-btn next"><img
                  src="<?php echo get_template_directory_uri() ?>/img/slider-arrow-right.svg" alt=""></a></li>
      </ul> <!-- .cd-timeline-navigation -->

      <div class="events-content">
         <?php if ( have_rows( 'geschichte' ) ) :  $i=0; ?>
         <ol>
            <?php while ( have_rows( 'geschichte' ) ) : the_row(); ?>
            <li <?php echo 'class="' . ($i==0 ? 'selected' : '') . '"'; ?>
               data-date="01/01/<?php the_sub_field( 'year' ); ?>">
               <div class="timeline-history">


               <?php if ( get_sub_field( 'image' ) ) { ?>
                  <div class="row horizontal-cart margin-b justify-content-center">
                     <div class="col-12 col-md-6 inner-content horizontal-shape">
                        <h2 class="uppercase color-red mb-5 year"><?php the_sub_field( 'year' ); ?></h2>
                        <?php the_sub_field( 'description' ); ?>
                     </div>
                     <div class="col-12 col-md-6 cover red-frame image"
                        style="background-image: url('<?php the_sub_field( 'image' ); ?>');">
                     </div>
                  </div>
                  <?php
               } else {
                  ?>
                  <div class="row horizontal-cart margin-b justify-content-center">
                     <div class="col-12 inner-content horizontal-shape">
                        <h2 class="uppercase color-red mb-5 year"><?php the_sub_field( 'year' ); ?></h2>
                        <?php the_sub_field( 'description' ); 
               } ?>

               </div>
            </li>
            <?php $i++;  ?>

            <?php endwhile; ?>
         </ol>
         <?php endif; ?>

      </div> <!-- .events-content -->
   </section>
</div>

<?php get_footer(); ?>