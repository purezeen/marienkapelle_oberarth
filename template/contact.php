<?php /* Template Name: Contact */ get_header(); ?>

<div class="move-content">

</div>

 <?php if ( have_rows( 'contact_form_and_informations' ) ) : ?>
  <div class="container contact">
    <div class="row">
    <?php while ( have_rows( 'contact_form_and_informations' ) ) : the_row(); ?>
      <?php the_sub_field( 'information' ); ?>
      <?php the_sub_field( 'form' ); ?>
    <?php endwhile; ?>
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
<?php endif; ?>

<!-- location map -->
<section class="container section relative line">
   <!-- <img class="line" src="<?php echo get_template_directory_uri() ?>/img/line.png" alt=""> -->
   <div class="row">
      <div class="col-12 map-wrap">
         <div id="map"></div>
      </div>
   </div>
   <!-- end row -->
</section>
<!-- end container -->


<?php get_footer(); ?>