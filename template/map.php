<?php /* Template Name: Map */ get_header(); ?>

<div class="move-content"></div>

<section class="map container section">
   <h2 class="text-center underline color-light-green mb-3"><?php the_field( 'heading' ); ?></h2>
   <?php if ( have_rows( 'map' ) ) : 
         $i=0;?>
      <?php while ( have_rows( 'map' ) ) : the_row(); ?>
      <h3 <?php echo 'class="text-center uppercase color-green mb-3 ' . ($i>=1 ? 'line' : ' pt-2') . '"'; ?>> <?php the_sub_field( 'title' ); ?></h3>
      <div class="map-border">
         <div class="map-shape">
            <?php the_sub_field( 'iframe' ); ?>
         </div>
      </div>
      <?php $i++;  ?>
      <?php endwhile; ?>
   <?php endif; ?>

</section>




<?php get_footer(); ?>