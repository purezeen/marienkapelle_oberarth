<?php /* Template Name: Events */ get_header(); ?>

<div class="move-content"></div>
<section id="calendar" class="container calendar calendar-3-col">
  <?php echo do_shortcode('[add_eventon cal_id="calendar" tiles="yes" tile_count="3" tile_bg="1" event_count="3" show_limit="yes" ]');
      ?>
</section>

<?php get_footer(); ?>