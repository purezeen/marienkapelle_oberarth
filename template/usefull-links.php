<?php /* Template Name: Usefull links */ get_header(); ?>


   <section class="container section usefull-links">
      <h1 class="section-title text-center">NÜTZLICHE LINKS</h1>

      <?php if ( have_rows( 'usefull_links' ) ) : ?>
         <?php while ( have_rows( 'usefull_links' ) ) : the_row(); ?>
         <a href="<?php the_sub_field( 'url' ); ?>" target="_blank">
         <?php $image = get_sub_field( 'image' ); ?>
                  <?php if ( $image ) { ?>
            <div class="row justify-content-center align-items-center align-items-md-stretch">
               <div class="col-12 col-sm-8 col-md-5 col-lg-3 image cover" style="background-image: url(<?php echo $image['url']; ?>)">
               </div>
               <div class="col-12 col-sm-8 col-md-7 col-lg-8 align-center usefull-link-shape">
               <?php } ?>
                  <h3 class="uppercase color-red mb-3"><?php the_sub_field( 'title' ); ?></h3>
                  <p><?php the_sub_field( 'excerpt' ); ?></p>
                  <div class="btn-link btn-right">Geschichte&nbsp; &nbsp;</div>
               </div>
            </div>
         </a>

         <?php endwhile; ?>
      <?php else : ?>
         <?php // no rows found ?>
      <?php endif; ?>

      </div>
      <!-- end row -->
   </section>
   <!-- end container -->



      
<?php get_footer(); ?>