<?php /* Template Name: Sermons */ get_header(); ?>


<?php the_field( 'marienkirche_oberarth' ); ?>


<section class="container section">
   <h1 class="section-title text-center">SERMONS</h1>

   <!-- <div class="filter">
      <div class="dropdown">
         <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">Dropdown </button>
         <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <button class="dropdown-item" type="button">Action</button>
            <button class="dropdown-item" type="button">Another action</button>
            <button class="dropdown-item" type="button">Something else here</button>
         </div>
      </div>


      <div class="dropdown">
         <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            Jahr
         </button>
         <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <button class="dropdown-item" type="button">2019</button>
            <button class="dropdown-item" type="button">2018</button>
            <button class="dropdown-item" type="button">2017</button>
         </div>
      </div>
   </div>
 -->
   <div class="sermons-list">
      <?php 
      // the query
      $args = array(
         'post_type' => 'sermons-post',
         'post_status' => 'publish',
         'orderby' => 'menu_order'
      );
      $the_query = new WP_Query( $args ); ?>
       
      <?php if ( $the_query->have_posts() ) : ?>
       
        <!-- pagination here -->

        <!-- the loop -->
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <div class="sermon-box <?php echo get_the_ID(); ?>">
          <div class="sermon-header">
            <span class="color-light-red date">
               <?php 
               if(get_field( 'subtitle' )) { the_field( 'subtitle' ); }
               if(get_field( 'set_date' ) && get_field( 'subtitle' )) { echo " - "; }
               if(get_field( 'set_date' )) { the_field( 'set_date' ); }
               ?>
               </span>
            <a href="#">
               <h3 class="color-red uppercase"><?php the_title(); ?></h3>
            </a>
            <?php if ( has_excerpt() ): ?>
               <p><?php the_excerpt(); ?></p>   
            <?php endif ?>
          </div>

            <div class="sermon-footer clearfix">

           <?php if ( have_rows( 'video_chose' ) ) : ?>
             <?php while ( have_rows( 'video_chose' ) ) : the_row(); ?>
              <?php $video_media = get_sub_field( 'video_media' ); ?>
              <?php $url_video = get_sub_field( 'url_video' ); ?>
              <?php if ($video_media || $url_video ): ?>
               <!-- VIDEO trigger -->
              <div data-toggle="modal" data-target="#video-<?php echo get_the_ID(); ?>">
                <a href="#" class="icon"><img src="<?php  echo get_template_directory_uri() ?>/img/video-icon.svg"
                    alt="Play"></a>
              <!-- MODAL Video -->
               <div class="modal fade modal-post" id="video-<?php echo get_the_ID(); ?>">
                 <button type="button" class="close" data-dismiss="modal"><img src="<?php echo get_template_directory_uri() ?>/img/close-img.svg" alt="close"></button>
                 <div class="modal-dialog ">
                    <div class="modal-content">
                       <div class="post-inner-content">
                              <?php if ($video_media): ?>
                                 <figure class="video-wrap">
                                    <video controls="" src="<?php echo $video_media['url']; ?>">
                                 </figure>  
                                 <a class="btn-pdf" href="<?php echo $video_media['url']; ?>" download>Herunterladen</a>
                              <?php else: ?> 
                                 <div class="video-wrap">
                                 <?php echo $url_video; ?>
                                 </div> 
                              <?php endif ?>
                       </div>
                    </div>
                 </div>
               </div>
              </div>
            <?php endif; ?>
               <?php endwhile; ?>
             <?php endif; ?>

               <?php if (get_field( 'audio' )): ?>
              <!-- PDF Trigger -->
              <div data-toggle="modal" data-target="#audio-<?php echo get_the_ID(); ?>">
               <a href="#" class="icon"><img src="<?php  echo get_template_directory_uri() ?>/img/play-icon.svg"
                    alt="Play"></a>
               <!-- MODAL PDF -->
               <div class="modal fade modal-post" id="audio-<?php echo get_the_ID(); ?>">
                 <button type="button" class="close" data-dismiss="modal"><img src="<?php echo get_template_directory_uri() ?>/img/close-img.svg" alt="close"></button>
                 <div class="modal-dialog ">
                    <div class="modal-content">
                       <div class="post-inner-content">
                          <figure class="wp-block-audio">
                           <?php $audio = get_field( 'audio' ); ?>
                              <audio controls="" src="<?php echo $audio['url']; ?>"></audio>
                           </figure>
                           <a class="btn-pdf" href="<?php echo $audio['url']; ?>" download>Herunterladen</a>
                       </div>
                    </div>
                 </div>
               </div>
              </div>
              <?php endif ?>

               <?php if (get_field( 'pdf' )): ?>
              <!-- PDF Trigger -->
               <a href="<?php the_field( 'pdf' ); ?>" class="icon" terget="_blank"><img src="<?php  echo get_template_directory_uri() ?>/img/pdf-icon.svg" alt="PDF"></a>
              <?php endif ?>

              <!-- Content trigger -->
              <?php if( '' !== get_post()->post_content ) : ?>
              <div data-toggle="modal" data-target="#the-content-<?php echo get_the_ID(); ?>">
                 <a href="#" class="btn-link btn-right">Geschichte</a>
                 <!-- MODAL content -->
                 <div class="modal fade modal-post" id="the-content-<?php echo get_the_ID(); ?>">
                    <button type="button" class="close" data-dismiss="modal"><img src="<?php echo get_template_directory_uri() ?>/img/close-img.svg" alt="close"></button>
                    <div class="modal-dialog ">
                       <div class="modal-content">
                          <div class="post-inner-content">
                             <span class="date"><?php echo get_the_date( "F d, Y" ); ?></span>
                             <h1 class='post-title uppercase text-center color-red'><?php the_title(); ?></h1>
                             <?php the_content(); ?>
                             <div class="post-inner-footer line">
                                <div class="post-author clearfix">
                                   <div class="post-author-wrap"><?php echo get_avatar( get_the_author_meta( 'ID' )); ?>
                                   </div>
                                   <?php $user_id = get_the_author_meta( 'ID' ); ?>
                                   <h3 class="color-red mb-0 mt-0"><?php the_author_meta( 'display_name', $user_id ); ?></h3>
                                   <p><?php echo nl2br(get_the_author_meta('description')); ?></p>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
                 
              </div>
            <?php endif; ?>
            </div>
         </div>
          <?php endwhile; ?>
          <!-- end of the loop -->
       
          <!-- pagination here -->
       
          <?php wp_reset_postdata(); ?>
          
      <?php endif; ?>
   </div>

</section>

<?php get_footer(); ?>