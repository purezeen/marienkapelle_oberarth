<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Adobe web font Active, Proxima Nova -->
<link rel="stylesheet" href="https://use.typekit.net/ndj6qec.css">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header" role="banner">
		<nav class="navbar navbar-expand-lg navbar-ligh container">
			<a href="/" class="navbar-brand">
				<img src="<?php echo get_template_directory_uri(); ?>/img/Maria_Immaculata_logo.svg"  alt="CoolBrand">
			</a>
			<button type="button"  data-toggle="collapse" data-target="#navbarCollapse" >
				<span class="navbar-toggler-icon"><span></span><span></span><span></span></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarCollapse">

			<?php wp_nav_menu(
				 array( 
					'theme_location' => 'primary', 
					'menu_id' => 'primary-menu',
					'container' => 'ul',
					'menu_class'=> 'navbar-nav ml-auto',
					'add_li_class'  => 'nav-item',
					 ) 
				); 
					?>
			</div>
		</nav>
		<!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="loader-wrapper">
		<div id="loader">
	</div>
				
		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>
	
	</div>

	<div id="content" class="site-content">
<?php $image = get_field( 'image' ); ?>
<?php if ( $image ) { ?>
	<!-- hero section -->
	<section class="hero cover" style="background-image: url(<?php echo $image['url']; ?>)">
	</section>
	<!-- end hero section -->
<?php } ?>
	
