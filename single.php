<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gulp-wordpress
 */

get_header(); ?>
<div class="move-content"></div>

<div id="primary" class="content-area single-post">
   <main id="main" class="site-main" role="main">
      <div class="container p-0">

         <div class="post-hero cover"
            style="background-image: url(<?php echo get_template_directory_uri()?>/img/hero-news.png)">
         </div>
         <div class="post-inner-content">
            <span class="date"> March 17, 2019</span>
            <?php
						
					while ( have_posts() ) : the_post();
						?>
            <h1 class='post-title uppercase text-center color-red'>
               <?php the_title(); ?>
            </h1>
            <?php
						the_content();
						get_template_part( 'template-parts/flexible', 'blog' );
								
					endwhile; // End of the loop.
					?>

            <div class="post-inner-footer line">
            <div class="post-author clearfix">
                                   <div class="post-author-wrap"><?php echo get_avatar( get_the_author_meta( 'ID' )); ?>
                                   </div>
                                   <?php $user_id = get_the_author_meta( 'ID' ); ?>
                                   <h3 class="color-red mb-0 mt-0"><?php the_author_meta( 'display_name', $user_id ); ?></h3>
                                   <p><?php echo nl2br(get_the_author_meta('description')); ?></p>
                                </div>
         </div>
      </div>

      <div class="news-page section">
         <div class="row">
            <div class="col-12">
               <h1 class="text-center section-title uppercase">ähnlich NEWS</h1>
            </div>
         </div>

         <?php
      $the_query = new WP_Query( array(
         'posts_per_page' =>3,
      )); 
      if ( $the_query->have_posts() ) : ?>
         <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

         <?php get_template_part( 'template-parts/content', 'page' ); ?>

         <?php endwhile; ?>
         <?php wp_reset_postdata(); ?>

         <?php else : ?>
         <p><?php __('No News'); ?></p>
         <?php endif; ?>

         <!-- end row -->
      </div>

   </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();